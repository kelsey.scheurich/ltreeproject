import "./musicPlayer.css";
import React from "react";
// @Todo: all of these image imports should be refactored into a nice dynamic thng
import songArt from "./components/assets/songArt.png";
import play from "./components/assets/icons/play.svg";
import spotify from "./components/assets/icons/spotify.svg";
import soundcloud from "./components/assets/icons/soundcloud.svg";
import bandcamp from "./components/assets/icons/bandcamp.svg";
import arrowImage from "./components/assets/icons/arrow.svg";

interface MusicPlayerProps {
  hidden: boolean;
  userColour: string;
}

// @Todo This whole thing needs to be refactored so it can use the drop down class, as well as getting data from API
export class MusicPlayer extends React.Component<MusicPlayerProps> {
  render() {
    return (
      <div className="musicPlayer" hidden={!this.props.hidden}>
        <div className="musicPlayer-container">
          <div className="musicPlayer-songContainer">
            <div className="musicPlayer-songArt">
              <img
                className="musicPlayer-songImg"
                src={songArt}
                alt="Freyas cool song"
              />
            </div>
            <div className="musicPlayer-songControls">
              {/* @Todo fix up this icon so it looks right */}
              <img src={play} style={{ height: "60px" }} alt="play button" />
            </div>
            <div className="musicPlayer-songTitle">
              Freya's cool song - Freya The Dog
            </div>
          </div>
          {/* @Todo ideally reafactor this into a nice function so you can just pass in the % */}
          <div className="musicPlayer-songProgress">
            <div
              style={{
                width: "60%",
                height: "100%",
                backgroundColor: this.props.userColour,
              }}
            ></div>
            <div
              style={{
                width: "100%",
                height: "100%",
                backgroundColor: " #dadee0",
              }}
            ></div>
          </div>
        </div>
        <div className="musicPlayer-listContainer">
          {/* @Todo reformat dropdown class so that the items can be used in here instead OR reformat these out into their own class */}
          <div className="musicPlayer-listItem">
            <div className="musicPlayer-listInner">
              <img
                className="musicPlayer-listIcon"
                src={spotify}
                alt="spotify"
              />
              <div className="musicPlayer-listItem-text">Spotify</div>
              <div className="musicPlayer-linkArea">
                <img
                  className="musicPlayer-listItem-arrow"
                  src={arrowImage}
                  alt="go to link"
                />
              </div>
            </div>
            <div className="musicPlayer-partialBoarder" />
          </div>
          <div className="musicPlayer-listItem">
            <div className="musicPlayer-listInner">
              <img
                className="musicPlayer-listIcon"
                src={soundcloud}
                alt="soundcloud"
              />
              <div className="musicPlayer-listItem-text">Soundcloud</div>
              <div className="musicPlayer-linkArea">
                <img
                  className="musicPlayer-listItem-arrow"
                  src={arrowImage}
                  alt="go to link"
                />
              </div>
            </div>
            <div className="musicPlayer-partialBoarder" />
          </div>
          <div className="musicPlayer-listItem">
            <div className="musicPlayer-listInner">
              <img
                className="musicPlayer-listIcon"
                src={bandcamp}
                alt="bandcamp"
              />
              <div className="musicPlayer-listItem-text">Bandcamp</div>
              <div className="musicPlayer-linkArea">
                <img
                  className="musicPlayer-listItem-arrow"
                  src={arrowImage}
                  alt="go to link"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
