import "./ltButton.css";
import React from "react";

interface ButtonProps {
  colour: string;
  text: string;
  onClick(): void;
}

// @Todo:
// - Pass func to button
// - Invert colour on hover
// - Remove placeholder text
export class LTButton extends React.Component<ButtonProps> {
  render() {
    return (
      <button
        className="listButton"
        style={{ backgroundColor: this.props.colour }}
        onClick={() => this.props.onClick()}
      >
        {this.props.text}
      </button>
    );
  }
}
