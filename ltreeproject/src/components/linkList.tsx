import "./linkList.css";
import React from "react";
import { LTButton } from "./ltButton";

interface LinkListProps {
  colour: string;
}

export class LinkList extends React.Component<LinkListProps> {
  // @Todo:
  // - hit API to get user's list of links etc
  // - format nicely and programmatically make list items instead of hardcoding
  render() {
    const listItems = this.getListItems();
    return <div className="linkList-Container">{listItems}</div>;
  }

  // @Todo:
  // - Replace fake links with actual things pulled with API
  getListItems() {
    const names = ["Instagram", "Twitter"];
    const links = ["http://instagram.com", "http://twitter.com"];

    return links.map((link, i) => (
      <LTButton
        colour={this.props.colour}
        text={names[i]}
        onClick={() => {
          window.open(link, "_blank");
        }}
      />
    ));
  }
}
