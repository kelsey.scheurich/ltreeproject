import React from "react";
import "./linkTreeFooter.css";
import logo from "./assets/logo.svg";

export class LinkTreeFooter extends React.Component {
  render() {
    return (
      <div className="linkTreeFooter-container">
        <img src={logo} alt="Linktree logo"></img>
      </div>
    );
  }
}
