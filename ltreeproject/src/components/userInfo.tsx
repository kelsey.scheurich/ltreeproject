import React from "react";
import "./userInfo.css";
import profilePic from "./assets/profile-picture.png";
// @Todo: maybe move images to somewhere that makes a bit more sense?

export class UserInfo extends React.Component {
  // @Todo:
  // - get user's actual username & pic
  render() {
    return (
      <div className="userInfo-container">
        <img src={profilePic} alt="profile"></img>
        <div className="userInfo-name">
          <span>@yodawg</span>
        </div>
      </div>
    );
  }
}
