import React from "react";
import "./dropDown.css";
import { DropDownItem } from "./dropDownItem";
import logo from "./assets/icons/by-songkick-wordmark.svg";

interface DropDownProps {
  hiddenState: boolean;
  footer?: boolean;
  icon?: boolean;
}

export class DropDown extends React.Component<DropDownProps> {
  render() {
    // @Todo:
    // - hit api for show information
    // - pull out date, location, link
    // - programmatically iterate over results to generate DropDownItems instead of hardcoding like I have
    return (
      <div className="dropDown" hidden={!this.props.hiddenState}>
        <div className="dropDown-container">
          {this.getListItems()}
          <div className="dropDown-footer">
            <img src={logo} alt="song kick logo" />
          </div>
        </div>
      </div>
    );
  }

  getListItems() {
    return (
      <>
        <DropDownItem
          showDate="April 30, 2022"
          subText="Venue Name, City"
          arrow={true}
        />
        <DropDownItem
          showDate="March 14, 2022"
          subText="Venue Name, City"
          arrow={true}
        />
        <DropDownItem
          showDate="December 1, 2021"
          subText="Venue Name, City"
          arrowText="Sold Out"
        />
        <DropDownItem
          showDate="October 31, 2021"
          subText="Venue Name, City"
          arrow={true}
        />
      </>
    );
  }
}
