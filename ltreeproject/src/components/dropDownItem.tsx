import React from "react";
import "./dropDownItem.css";
import arrowImage from "./assets/icons/arrow.svg";

interface DropDownItemProps {
  showDate: string;
  subText?: string;
  arrow?: boolean;
  arrowText?: string;
}

// @Todo genericize(?) this so that the text area etc can be passed in/adapted to different styles of dropdown needed
// Also add the needed link to the props
// Ideally it should be flexible enough to add and remove components like icons as neccessary
export class DropDownItem extends React.Component<DropDownItemProps> {
  render() {
    return (
      <>
        <div className="dropDownItem-container">
          <div className="dropDownItem-textArea">
            <div className="dropDownItem-mainText">{this.props.showDate}</div>
            <div className="dropDownItem-subText">{this.props.subText}</div>
          </div>
          <div className="dropDownItem-linkArea">
            {this.props.arrow ? (
              <img
                className="dropDownItem-arrow"
                src={arrowImage}
                alt="go to link"
              />
            ) : (
              this.props.arrowText
            )}
          </div>
        </div>
        <div className="dropDownItem-partialBoarder" />
      </>
    );
  }
}
