import React from "react";
import { LinkList } from "./components/linkList";
import { ShowsList } from "./showsList";
import "./linkContainer.css";
import { MusicList } from "./musicList";

interface LinkContainerProps {
  userColour: string;
}

// @Todo depending on how much stuff ends up in here it could be worth creating a view state store
export enum ViewState {
  none,
  music,
  shows,
}

// @Todo this would be what gets replaced by a proper router.
// Personally I would use react router
export class LinkContainer extends React.Component<LinkContainerProps, any> {
  constructor(props: LinkContainerProps) {
    super(props);
    this.setViewState = this.setViewState.bind(this);
    this.state = {
      viewState: ViewState.none,
    };
  }

  setViewState(vs: ViewState) {
    if (vs === this.state.viewState) {
      // If they have clicked the same thing twice, toggle it
      vs = ViewState.none;
    }
    this.setState({ viewState: vs });
  }

  render() {
    return (
      <div className="linkContainer">
        <LinkList colour={this.props.userColour} />
        <MusicList
          userColour={this.props.userColour}
          showList={this.state.viewState === ViewState.music}
          setViewState={this.setViewState}
        />
        <ShowsList
          userColour={this.props.userColour}
          showList={this.state.viewState === ViewState.shows}
          setViewState={this.setViewState}
        />
      </div>
    );
  }
}
