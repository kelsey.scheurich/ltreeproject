import React from "react";
import { LTButton } from "./components/ltButton";
import { ViewState } from "./linkContainer";
import "./musicList.css";
import { MusicPlayer } from "./musicPlayer";

interface MusicListProps {
  userColour: string;
  showList: boolean;
  setViewState(vs: ViewState): void;
}

// @Todo: This music list class, the shows list class, and the classic link list class could be refactored out into a list factory maybe
export class MusicList extends React.Component<MusicListProps> {
  render() {
    return (
      <div className="musicList-container">
        <LTButton
          colour={this.props.userColour}
          text="Music Player"
          onClick={() => this.props.setViewState(ViewState.music)}
        />
        <MusicPlayer
          userColour={this.props.userColour}
          hidden={this.props.showList}
        />
      </div>
    );
  }
}
