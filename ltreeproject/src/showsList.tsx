import React from "react";
import { DropDown } from "./components/dropDown";
import { LTButton } from "./components/ltButton";
import { ViewState } from "./linkContainer";
import "./showsList.css";

interface ShowsListProps {
  userColour: string;
  showList: boolean;
  setViewState(vs: ViewState): void;
}

export class ShowsList extends React.Component<ShowsListProps> {
  // @Todo:
  // - get list of shows
  render() {
    return (
      <div className="showsList-container">
        <LTButton
          colour={this.props.userColour}
          text="Shows"
          onClick={() => this.props.setViewState(ViewState.shows)}
        />
        <DropDown hiddenState={this.props.showList} />
      </div>
    );
  }
}
