import React from "react";
import "./App.css";
import { LinkTreeFooter } from "./components/linkTreeFooter";
import { UserInfo } from "./components/userInfo";
import { LinkContainer } from "./linkContainer";

// @Todo:
// - get user info from API somewhere
const userColour = "#39E09B";

// @Todo:
// - replace linkList with the appropriate content for the state
// - also maybe add a state
const App: React.FC = (): JSX.Element => {
  return (
    <div className="App">
      <UserInfo />
      {/* @Todo replace with router */}
      <LinkContainer userColour={userColour} />
      <LinkTreeFooter />
    </div>
  );
};

export default App;
