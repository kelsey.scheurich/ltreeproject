// @Todo:
// - all of these would have to be mocked-up correctly once data started coming from APIs instead of being hard coded
describe('My cool Linktree app', () => {
    beforeEach(() => {
        cy.visit('http://localhost:3000/');
    });
    // "When a user clicks on a Music Player Link, the Spotify streaming platform is visible"
    it('',() => {
        // click on music player
        cy.get('button').contains('Music Player').click();

        // Should show spotify platform
        cy.contains('Spotify').should('be.visible');
    });

    // "When a user clicks on the Shows List Link, a list of X shows are visible"
    it('',() => {
        // Click on shows list link
        cy.get('button').contains('Shows').click();

        // Should show at least one show information
        cy.contains('Venue');
    });

    // When a user clicks on the Music Player Link and then on a Shows List Link, the Music Player Link closes"
    it('',() => {
        // click on music player
        cy.get('button').contains('Music Player').click();

        // Should show spotify platform
        cy.contains('song').should('be.visible');

        // Click on shows list link
        cy.get('button').contains('Shows').click();

        // Should close music link
        cy.contains('song').should('not.be.visible');

        // Should show at least one show information
        cy.contains('Venue');
    });
});

