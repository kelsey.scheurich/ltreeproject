# LTreeProject

Hello! Welcome to my super cool Linktree project. Please enjoy! Here are some tips and/or tricks to help you out.

Please note, this project uses `npm` so please make sure you have it installed.

## How to run the tests

There are some basic integration tests that can be run by cypress. These cover the three scenarios that were mentioned in the requirements.

## To run cypress:

_Prerequisite: Cypress uses Chrome, so please make sure it is installed._

1. Open a terminal within the `/ltreeproject` folder of your repo and run `npx cypress open`
2. In the Cypress window you will see a list of Integration Tests. Click on `Basic.spec.js` or the 'Run Integration" button to run the tests.
3. Cypress will automagically open and control a chrome window. It can take a bit of time, so sit back and watch the magicalness unfold!

## To run the project:

To run this project, open the project folder in your editor of choice (I used VSCode) or a new terminal, navigate to the `/ltreeproject` folder, and run `npm start`.

Enjoy 😻

_Note: one of my @Todo things is to remove all the automatically generated default cruft you get when using things like react_
